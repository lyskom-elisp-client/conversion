import os
import sys
import shutil

RCS_WHITESPACE = " \x08\t\n\v\f\r"

# We should maybe add "." here as well, but than we would have to add
# more special-casing to get revisions as a single token.  This
# definition is good enough for our purposes, but not for a general
# RCS parser.
RCS_PUNCTUATION = "$,:;"

RCS_SPECIAL = RCS_WHITESPACE + RCS_PUNCTUATION + "@"

class EofInString(Exception):
    pass

class Buffer(object):
    def __init__(self, filename):
        self.__fp = open(filename)
        self.__buf = ""
        self.__eof = False
        self.__filename = filename

    def filename(self):
        return self.__filename

    def getc(self):
        c = self.peek()
        if c != "":
            self.__buf = self.__buf[1:]
        return c

    def ungetc(self, c):
        self.__buf = c + self.__buf

    def fillbuffer(self, bytes):
        if self.__eof or len(self.__buf) >= bytes:
            return
        s = self.__fp.read(max(1024, bytes))
        if s == "":
            self.__eof = True
        self.__buf += s

    def peek(self, bytes=1):
        self.fillbuffer(bytes)
        return self.__buf[:bytes]

    def lookahead(self, allowed):
        c = self.peek()
        return len(c) > 0 and c in allowed

    def negative_lookahead(self, allowed):
        c = self.peek()
        return len(c) > 0 and c not in allowed

    def eof(self):
        return self.__eof and self.__buf == ""

class Lexer(object):
    def __init__(self, buffer):
        self.__buffer = buffer
        self.__token = None

    def filename(self):
        return self.__buffer.filename()

    def next(self):
        res = self.peek()
        self.__token = self.scan()
        return res

    def peek(self):
        if self.__token is None:
            self.__token = self.scan()
        return self.__token

    def scan(self):
        c = self.__buffer.getc()
        if c == "":
            return Eof()
        if c == "@":
            return String(readstring(self.__buffer))
        if c in RCS_WHITESPACE:
            res = c
            while self.__buffer.lookahead(RCS_WHITESPACE):
                res += self.__buffer.getc()
            return Whitespace(res)
        if c in RCS_PUNCTUATION:
            return Punctuation(c)
        res = c
        while self.__buffer.negative_lookahead(RCS_SPECIAL):
            res += self.__buffer.getc()
        return Word(res)

class Token(object):
    def __init__(self, s):
        self.__s = s

    def replace_content(self, s):
        self.__s = s

    def content(self):
        return self.__s

    def format(self):
        return self.content()

class Eof(object):
    pass

class String(Token):
    def format(self):
        return "@" + self.content().replace("@", "@@") + "@"

    def dwim_encoding(self, filename, revision):
        ascii = None
        try:
            ascii = self.content().decode("ascii")
        except UnicodeDecodeError:
            pass
        latin_1 = None
        try:
            latin_1 = self.content().decode("latin_1")
        except UnicodeDecodeError:
            pass
        utf_8 = None
        try:
            utf_8 = self.content().decode("utf_8")
        except UnicodeDecodeError:
            pass

        # Convert latin-1 to utf-8.
        if latin_1 is not None and utf_8 is None and ascii is None:
            self.replace_content(latin_1.encode("utf_8"))
            return

        # Convert ISO 646-SE to utf-8.
        if ascii is not None:
            s = ascii.replace("}", u"\u00e5")
            s = s.replace("{", u"\u00e4")
            s = s.replace("|", u"\u00f6")
            s = s.replace("]", u"\u00c5")
            s = s.replace("[", u"\u00c4")
            s = s.replace("\\", u"\u00d6")
            if s != ascii:
                print filename + ":" + revision
                print s.encode("utf_8")
                print "-" * 70
                self.replace_content(s.encode("utf_8"))
                return

class Whitespace(Token): pass
class Punctuation(Token): pass
class Word(Token): pass

def readstring(fp):
    res = ""
    while True:
        c = fp.getc()
        if c == "":
            raise EofInString(fp.filename())
        if c == "@":
            if fp.peek() != "@":
                return res
            else:
                c = fp.getc()
        res += c

def copyfile(fp, outfp):
    global stoplist

    seen = []
    while not isinstance(fp.peek(), Eof):
        token = fp.next()
        if (len(seen) > 3 and
            isinstance(seen[0], Word) and
            isinstance(seen[1], Whitespace) and
            isinstance(seen[2], Word) and seen[2].content() == "log" and
            isinstance(seen[3], Whitespace) and
            isinstance(token, String) and
            not fp.filename() + ":" + seen[0].content() in stoplist):

            token.dwim_encoding(fp.filename(), seen[0].content())
        outfp.write(token.format())
        seen.append(token)
        seen = seen[-4:]
    outfp.close()

def recurse(src, dst):
    for name in sorted(os.listdir(src)):
        srcfile = os.path.join(src, name)
        dstfile = os.path.join(dst, name)
        if os.path.isdir(srcfile):
            os.mkdir(dstfile)
            recurse(srcfile,
                    dstfile)
        elif os.path.isfile(srcfile):
            if srcfile.endswith(",v"):
                fp = open(dstfile, "w")
                copyfile(Lexer(Buffer(srcfile)), fp)
                fp.close()
            else:
                shutil.copyfile(srcfile, dstfile)

def load_stoplist():
    global stoplist

    stoplist = set()
    for line in open("commitmsg-nodwim.txt", "r"):
        stoplist.add(line.strip())

def main():
    load_stoplist()
    recurse(sys.argv[1], sys.argv[2])

if __name__ == '__main__':
    main()

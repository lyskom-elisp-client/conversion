set -e
export GIT_COMMITTER_EMAIL=ceder@lysator.liu.se
export TERM=dumb
GIT="git --no-pager"

rm_original()
{
    git rev-parse --symbolic --glob=original \
    | while read ref
      do
	 git update-ref -d $ref
      done
}

fix_grafts ()
{
    git checkout master || exit
    git filter-branch --tag-name-filter cat -- --all || exit
    rm_original
    rm -f .git/info/grafts || exit
}

# $1: commit.  The files are removed in this commit and all newer commits.
# $2: files to remove, as a whitespace-separated list.
# $3: until. Optional. If supplied, files are removed only until this commit.
filter_rm ()
{
    test -f .git/info/grafts && fix_grafts
    if [ $# -eq 2 ]
    then
	commit="$1"
	files="$2"
	until=
	untilrev=
	untilrevparent=
	tags=`$GIT tag --contains $($commit)`
	branches=`$GIT branch -a --contains $($commit)|sed s/..//`
    else
	commit="$1"
	until="$2"
	files="$3"
	untilrev=$($until)
	untilrevparent=$($until)^
	$GIT branch tmp-filter-head $untilrev
	$GIT branch tmp-filter-parent $untilrevparent
	tags=`($GIT tag --contains $($commit);$GIT tag --contains $($until))\
               |sort|uniq -c|sed -n 's/^ *1 //p'`

	branches=`($GIT branch -a --contains $($commit)|sed s/..//; \
		   $GIT branch -a --contains $($until)|sed s/..//) \
	          |sort|uniq -c|sed -n 's/^ *1 //p'`
	branches="$branches tmp-filter-parent"
    fi

    $GIT checkout master
    $GIT filter-branch \
	--tag-name-filter cat \
	--index-filter "git rm --cached -q --ignore-unmatch $files" \
	-- $tags $branches \
	--not $($commit)^
    #  master..svn/isc master..svn/python master..svn/tcl
    rm_original

    if [ -n "$until" ]
    then
	echo `$GIT rev-parse tmp-filter-head` \
	    `$GIT rev-parse tmp-filter-parent` >> .git/info/grafts
	fix_grafts
	$GIT branch -D tmp-filter-head
	$GIT branch -D tmp-filter-parent
    fi
}

# Fix file renames that were made by copying the ,v file and then
# removing the old file.
#
# $1: commit where the file was renamed.
# $2: the old name.
# $3: the new name.
fixup_dup_rename ()
{
    test -f .git/info/grafts && fix_grafts
    commit="$1"
    old="$2"
    new="$3"

    $GIT checkout master
    $GIT filter-branch \
	--tag-name-filter cat \
	--index-filter "if git merge-base --is-ancestor $commit \$GIT_COMMIT;
 then git rm --cached -q --ignore-unmatch $old;
 else git rm --cached -q --ignore-unmatch $new;
 fi" -- --all
    rm_original
}

gc ()
{
    rm_original
    git reflog expire --all --expire=now
    git gc --aggressive --prune="0 seconds ago"
}

assert_same_rev() {
    r1=`git rev-parse $1^0`
    r2=`git rev-parse $2^0`
    if [ "$r1" != "$r2" ]
    then
	echo revs differ: $1=$r1, but $2=$r2 >&2
	exit 1
    fi
}

add_missing_branch() {
    git rev-parse --verify -q $1 || git branch $1 $2
    assert_same_rev $1 $2
}

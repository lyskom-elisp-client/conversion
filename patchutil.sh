setup()
{
    VERSION=$1
    VERSION_DOT=`echo $1|sed s/-/./g`
    cd facit/ftpko/$VERSION || exit 1
}

copyfile()
{
    dst=$1
    # If the src directory is missing, remove any "src/" prefix (if present).
    test -d src || dst=${dst#src/}
    mkdir -p `dirname "$dst"`
    cp ../../../fromgit/v$VERSION/$1 $dst
}

rm_vars_el()
{
    sed s/@@CLIENTVERSION@@/$VERSION_DOT/g vars.el.in > vars.el.2
    cmp vars.el vars.el.2 || exit 1
    rm -f vars.el vars.el.2
}

# Main entry point.
all: stamp/final

# To run the verifications from oldest to newest, call make this
# target once.  The effect persists until you manually remove
# cmpdeps.mk.
compare-in-order:

-include cmpdeps.mk

BRANCHES = master v0-38-2 v0-45-1-maint

OTHERTAGS = server-1-0 server-1-0-1 server-1-0-3 server-1-1-0 \
	server-1-5-signumbeta server-1-8-fixes \
	tty-0-03 v0-31-8 v-0-31-9

TAGS = v0-34 v0-34-1 v0-34-2 v0-34-3 v0-34-4 v0-34-5 v0-34-6 \
	v0-35-alfa \
	v0-36 \
	v0-37 \
	v0-38 \
	v0-39 \
	v0-40 v-40-1 \
	v0-41 \
	v0-42 \
	v0-43 v0-43-1 v0-43-2 \
	v0-44 v0-44-1 \
	v0-45 v0-45-1 v0-45-1-P1 v0-45-2 \
	v0-46-BETA-1 v0-46-BETA-2 v0-46-BETA-3 v0-46-BETA-4 v0-46-BETA-5 \
	v0-46-BETA-6 v0-46-BETA-7 v0-46-BETA-8 v0-46-BETA-9 v0-46-BETA-A \
	v0-46-BETA-B v0-46-BETA-C v0-46-BETA-D v0-46-BETA-E v0-46-BETA-F \
	v0-46-BETA-G v0-46-BETA-H v0-46-BETA-I v0-46-BETA-J v0-46-BETA-K \
	v0-46-BETA-L v0-46-BETA-M v0-46-BETA-N v0-46-BETA-O v0-46-BETA-P \
	v0-46-BETA-Q v0-46-BETA-W \
	v0-46 \
	v0-46-1-BETA-1 v0-46-1-BETA-2 v0-46-1-BETA-3 \
	v0-46-1-BETA-4 v0-46-1-BETA-5 \
	v0-46-1 \
	v0-46-2-BETA-1 v0-46-2-BETA-2 \
	v0-47-BETA-1 v0-47-BETA-2 v0-47-BETA-3 v0-47-BETA-4 \
	v0-47 \
	v0-47-1-BETA-1 v0-47-1-BETA-2 \
	v0-47-1 \
	v0-48

REFS = $(BRANCHES) $(TAGS)

all: $(addprefix stamp/compare-,$(REFS))

compare-in-order: reforder.txt
	prev=; \
	while read curr; \
	do \
	    if [ -n "$$prev" ]; then \
	        echo stamp/compare-$$curr: stamp/compare-$$prev; \
	    fi; \
	    prev=$$curr; \
	done < reforder.txt > cmpdeps.mk.tmp
	mv cmpdeps.mk.tmp cmpdeps.mk
	$(MAKE)

stamp/cvsroot:
	rm -rf cvsroot
	scp -r cvs.lysator.liu.se:/cvsroot/lyskom-elisp-client cvsroot
	mkdir -p stamp
	touch $@

stamp/cvsroot-utf-8: stamp/cvsroot commitmsg-dwim.py commitmsg-nodwim.txt
	rm -rf cvsroot-utf-8
	mkdir cvsroot-utf-8
	python commitmsg-dwim.py cvsroot cvsroot-utf-8 > dwimmed-iso-646.txt
	touch $@

stamp/cvsroot-extratags: stamp/cvsroot-utf-8 extratags.txt set-tags extrabranches.txt set-branches
	rm -rf cvsroot-extratags
	cp -a cvsroot-utf-8 cvsroot-extratags
	./set-tags cvsroot-extratags extratags.txt
	./set-branches cvsroot-extratags extrabranches.txt
	touch $@

stamp/cvsroot-sans-petlis-mailhack: stamp/cvsroot-extratags rm-petli
	rm -rf cvsroot-sans-petlis-mailhack
	cp -a cvsroot-extratags cvsroot-sans-petlis-mailhack
	./rm-petli cvsroot-sans-petlis-mailhack
	touch $@

stamp/cvsroot-fixed: stamp/cvsroot-sans-petlis-mailhack
	rm -rf cvsroot-fixed
	cp -a cvsroot-sans-petlis-mailhack cvsroot-fixed
	touch $@

stamp/00: stamp/cvsroot-fixed authormap.txt gitconversion
	./gitconversion
	touch $@

stamp/10: stamp/00 clone fixes/10-rm-Mailing-do-list
	./clone $< $@
	cd 10.git && ../fixes/10-rm-Mailing-do-list
	touch $@

stamp/11: stamp/10 clone fixes/11-rm-elisp-doc
	./clone $< $@
	cd 11.git && ../fixes/11-rm-elisp-doc
	touch $@

stamp/12: stamp/11 clone fixes/12-mv-todo
	./clone $< $@
	cd 12.git && ../fixes/12-mv-todo
	touch $@

stamp/13: stamp/12 clone fixes/13-rm-lyskom-send
	./clone $< $@
	cd 13.git && ../fixes/13-rm-lyskom-send
	touch $@

stamp/14: stamp/13 clone fixes/14-rm-doc-changelog
	./clone $< $@
	cd 14.git && ../fixes/14-rm-doc-changelog
	touch $@

stamp/15: stamp/14 clone fixes/15-rm-39
	./clone $< $@
	cd 15.git && ../fixes/15-rm-39
	touch $@

stamp/16: stamp/15 clone fixes/16-vars-el
	./clone $< $@
	cd 16.git && ../fixes/16-vars-el
	touch $@

stamp/17: stamp/16 clone fixes/17-v0-45-1-maint
	./clone $< $@
	cd 17.git && ../fixes/17-v0-45-1-maint
	touch $@

stamp/18: stamp/17 clone fixes/18-makefile-news
	./clone $< $@
	cd 18.git && ../fixes/18-makefile-news
	touch $@

stamp/97: stamp/18 clone fixes/97-empty-msg
	./clone $< $@
	cd 97.git && ../fixes/97-empty-msg
	touch $@

stamp/98: stamp/97 clone
	./clone $< $@
	cd 98.git && git branch -d origin
	cd 98.git && git tag v0-40-1 v-40-1
	touch $@

stamp/99: stamp/98 clone
	./clone $< $@
	touch $@

stamp/final: stamp/99 untag renametag
	./clone $< $@
	./untag
	./renametag
	touch $@

stamp/fromcvs-%: stamp/cvsroot-fixed cvs-branch
	./cvs-branch $*
	touch $@

stamp/fromgit-%: stamp/99 git-clone
	./git-clone $*
	touch $@

stamp/compare-%: stamp/fromcvs-% stamp/fromgit-% compare
	./compare $*
	touch $@

reforder.txt: stamp/00
	for a in $(REFS); do \
	    echo Processing $$a... >&2 ; \
	    for b in $(REFS); do \
		(cd 00.git && \
		 git merge-base --is-ancestor $$a $$b && \
		 ! git merge-base --is-ancestor $$b $$a && \
		 echo $$a $$b); \
	    done; \
	done | tsort > $@.tmp
	mv $@.tmp $@

.PRECIOUS: stamp/compare-%
.PRECIOUS: stamp/fromcvs-%
.PRECIOUS: stamp/fromgit-%

FTP=ftp://ftp.lysator.liu.se/pub/lyskom/elisp-client

ftp/elisp-client-0-39.tar.gz:
	mkdir -p ftp
	curl -o $@.tmp $(FTP)/old/src/elisp-client-0.39.tar.gz
	mv $@.tmp $@

ftp/elisp-client-0-40.tar.gz:
	mkdir -p ftp
	curl -o $@.tmp $(FTP)/old/src/elisp-client-0.40.tar.gz
	mv $@.tmp $@

ftp/elisp-client-0-40-1.tar.gz:
	mkdir -p ftp
	curl -o $@.tmp $(FTP)/old/src/elisp-client-0.40.1.tar.gz
	mv $@.tmp $@

ftp/elisp-client-0-41.tar.gz:
	mkdir -p ftp
	curl -o $@.tmp $(FTP)/old/src/elisp-client-0.41.tar.gz
	mv $@.tmp $@

ftp/elisp-client-0-43.tar.gz:
	mkdir -p ftp
	curl -o $@.tmp $(FTP)/old/src/elisp-client-0.43.tar.gz
	mv $@.tmp $@

ftp/elisp-client-0-47.tar.gz:
	mkdir -p ftp
	curl -o $@.tmp $(FTP)/old/lyskom-elisp-client-src-0.47.tar.gz
	mv $@.tmp $@

ftp/elisp-client-0-47-1.tar.gz:
	mkdir -p ftp
	curl -o $@.tmp $(FTP)/old/lyskom-elisp-client-src-0.47.1.tar.gz
	mv $@.tmp $@

ftp/elisp-client-0-48.tar.gz:
	mkdir -p ftp
	curl -o $@.tmp $(FTP)/lyskom-elisp-client-src-0.48.tar.gz
	mv $@.tmp $@

FTP_SRC_VERSIONS = 0-39 0-40 0-40-1 0-41 0-43
FTP_DIST_VERSIONS = 0-47 0-47-1 0-48

all: $(addprefix stamp/ftpcmp-,$(FTP_SRC_VERSIONS))
all: $(addprefix stamp/ftpdistcmp-,$(FTP_DIST_VERSIONS))

stamp/ftp-%: ftp/elisp-client-%.tar.gz
	rm -rf facit/ftp/$*
	mkdir -p stamp facit/ftp/$*
	tar -C facit/ftp/$* --strip-components=1 -x -f $^ -z
	touch $@

.PRECIOUS: stamp/ftp-%

stamp/ftpko-%: stamp/ftp-% fixko patchko-% stamp/fromgit-v% patchutil.sh
	rm -rf facit/ftpko/$*
	mkdir -p facit/ftpko
	cp -a facit/ftp/$* facit/ftpko/$*
	./fixko facit/ftpko/$*
	./patchko-$*
	touch $@

.PRECIOUS: stamp/ftpko-%

stamp/ftpcmp-%: stamp/ftpko-% stamp/fromgit-v% compare-ftp
	./compare-ftp $*
	touch $@

.PRECIOUS: stamp/ftpcmp-%

stamp/ftpdistcmp-%: stamp/ftpko-% stamp/unpacked-dist-% compare-ftpdist
	./compare-ftpdist $*
	touch $@

.PRECIOUS: stamp/ftpdistcmp-%

stamp/unpacked-dist-%: stamp/99 fixdist
	./fixdist $*
	touch $@

.PRECIOUS: stamp/unpacked-dist-%
